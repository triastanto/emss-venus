<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wfh;
use App\Models\WfhCheck;
use Illuminate\Support\Facades\Auth;
use App\Models\SAP\Family;
use GuzzleHttp\Client;

use function GuzzleHttp\json_decode;

class EabsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $family = Family::sapOfLoggedUser()
            ->select([
                'T591S_STEXT', 'FCNAM', 'FGBDT', 'FGBOT', 'FASEX',
                'FASEX_DESC', 'KDZUG', 'AEDTM', 'ID'
            ])
            ->get();
            
        $personnel_no = Auth::user()->employee->personnel_no;
        $data = Wfh::where('personnel_no', $personnel_no)
        ->whereIn("time_event_type_id", [3,4])
        ->orderBy("id","desc")
        ->paginate(10);
        return view('e_absence.index', compact("data","family"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('time_in') ) {
            $type = 3;
        }

        if ($request->input('time_out') ) {
            $type = 4;
        }


        $tm = new Wfh();
        $tm->personnel_no = Auth::user()->personnel_no;
        $tm->check_date = date('Y-m-d');
        $tm->check_time = date('H:i:s');
        $tm->time_event_type_id =  $type;
        $tm->location = $request->input('location');
        $tm->latitude = $request->input('latitude');
        $tm->longitude = $request->input('longitude');
        $tm->save();

        return redirect()->back()->with("wfh_id",$tm->id);
    }

    public function wfhCheck(Request $request)
    {
        $wfh_id = base64_decode(base64_decode($request->wfh_id));
        foreach ($request->keluarga as $key => $value) {
            $wfhCheck = new WfhCheck();
            $wfhCheck->wfh_id = $wfh_id;
            $wfhCheck->keluarga = $value;
            $wfhCheck->keterangan = $request->keterangan[$key];
            $wfhCheck->gejala = $request->gejala[$key];
            $wfhCheck->save();
        }
        return redirect()->back()->with('success','Checkup Wfh Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function location($long, $lat)
    {
        // $user = Auth::user();
        // if ($user->id >= 1 && $user->id <= 1666) {
        //     $apiKey = 'pk.eyJ1IjoibmVuYXJlc3RpIiwiYSI6ImNrZG54aXBqazBnbXkyeHNheHBocm5lbDAifQ.VZWbnzZ1C_3wZ5u9OjplFQ';
        // }
        // if ($user->id >= 1667 && $user->id <= 2666) {
        //     $apiKey = 'pk.eyJ1Ijoic3VtaXlhdGktYSIsImEiOiJja2RueTA1Z2ExbjMxMnJvZnZneThwbXZzIn0.BSM0m_Q0DnYum3pAhgtHSw';
        // }
        // if ($user->id >= 2667) {
        //     $apiKey = 'pk.eyJ1IjoibmVuYXJlc3RpIiwiYSI6ImNrZG54aXBqazBnbXkyeHNheHBocm5lbDAifQ.VZWbnzZ1C_3wZ5u9OjplFQ';
        // }

        // // Create a client with a base URI
        // $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.mapbox.com/']);
        // $response = $client->request('GET', 'geocoding/v5/mapbox.places/'.$long.','.$lat.'.json?access_token='.$apiKey);
        // $body = $response->getBody();
        
        //$data = json_decode($body,true);
        return [];

        // Menggunakan nominatim.openstreetmap.org untuk merubah lat lon ke text lokasi
        // Create a client with a base URI

        // $headers = ['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'];
        // $client = new \GuzzleHttp\Client(['base_uri' => 'https://nominatim.openstreetmap.org']);
        // $response = $client->request('GET', 'reverse?lat='.$lat.'&lon='.$long.'&format=json',[
        //     'headers' => $headers
        // ]);
        // $body = $response->getBody();

        // $data = json_decode($body,true);
        // return $data;
    }
}
