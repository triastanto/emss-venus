<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\AllWfhDataTable;
use App\Models\Stage;
use App\Models\Wfh;

class AllWfhController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AllWfhDataTable $dataTable)
    {
        $stages = Stage::all();
        $foundYears = Wfh::foundYear()->get();

        return $dataTable->render('all_wfhs.index', 
            [ "stages" => $stages, "foundYears" => $foundYears ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function integrate(Request $request, $id)
    {
        // cari berdasarkan id kemudian update berdasarkan request + status reject
        $absence = Absence::find($id);
        
        // dispatch event agar dapat dimasukkan ke dalam job
        event(new SendingAbsenceToSap($absence));
        
        // tampilkan pesan bahwa telah berhasil 
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Integrasi sedang dilakukan. Silahkan cek email."
        ]);

        // kembali lagi ke index
        return redirect()->route('all_leaves.index');
    }

    public function confirm(Request $request, $id)
    {
        // cari berdasarkan id kemudian update berdasarkan request + status reject
        $absence = Absence::find($id);
        $absence->stage_id = Stage::successStage()->id;
        
        if (!$absence->save()) {
            // kembali lagi jika gagal
            return redirect()->back();
        }

        // tampilkan pesan bahwa telah berhasil menolak
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Data cuti berhasil dikonfirmasi masuk ke SAP."
        ]);

        // kembali lagi ke all leaves
        return redirect()->route('all_leaves.index');
    }   
}
