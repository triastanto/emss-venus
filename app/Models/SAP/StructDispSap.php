<?php

namespace App\Models\SAP;

use Illuminate\Database\Eloquent\Model;

class StructDispSap extends Model
{
    
    protected $table = 'structdisp_sap';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    public function scopeSelfStruct($query)
    {
        // struct untuk personnel_no
        return $query->where('no', 1);
    }

 }
