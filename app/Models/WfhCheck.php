<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WfhCheck extends Model
{
    public function wfh()
    {
        // many-to-one relationship dengan wfh
        return $this->belongsTo('\App\Models\Wfh');
    }
}
