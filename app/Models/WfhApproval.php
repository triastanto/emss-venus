<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ReceiveStatus;
use App\Traits\ParentStage;
use App\Traits\OfLoggedUserApproval;

class WfhApproval extends Model
{
    use ReceiveStatus, ParentStage, OfLoggedUserApproval;

    public $fillable = [
        'wfh_id',
        'regno',
        'sequence',
        'status_id',
        'text',
    ];
    
    protected $casts = [
        'id' => 'integer',
        'wfh_id' => 'integer',
        'regno' => 'integer',
        'sequence' => 'integer',
        'status_id' => 'integer',
        'text' => 'string'
    ];

    public static $rules = [

    ];

    public function user()
    {
        // many-to-one relationship dengan User
        return $this->belongsTo('App\User', 'regno', 'personnel_no');
    }

    public function employee()
    {
        // many-to-one relationship dengan Employee
        return $this->belongsTo('App\Models\Employee', 'regno', 'personnel_no');
    }

    public function wfh()
    {
        // many-to-one relationship dengan wfh
        return $this->belongsTo('\App\Models\Wfh');
    }

    public function status()
    {
        // one-to-one relationship dengan status
        return $this->belongsTo('\App\Models\Status');
    }

}
