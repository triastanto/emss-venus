<?php

namespace App\Observers;

use Session;
use App\Models\Wfh;
use App\Models\WfhApproval;
use App\Models\FlowStage;
use App\Models\Status;
use App\Role;
use App\Notifications\WfhDeletedMessage;

class WfhObserver
{
    public function creating(Wfh $wfh)
    {
        $msg = "Tidak dapat absen WFH karena tanggal pengajuan ";
        
        // mencari pengajuan TimeEvent pada tanggal yang sama
        $intersected = Wfh::where('personnel_no', $wfh->personnel_no)
            ->where('check_date', $wfh->check_date)
            ->where('time_event_type_id', $wfh->time_event_type_id)
            ->with(['timeEventType'])
            ->orderBy('id','desc')
            ->first();
        
        // apakah tanggal tidak slash sudah pernah dilakukan sebelumnya (intersection)
        if ( $intersected && !$intersected->is_denied) {

            // apakah timeEventType yang akan disimpan sama dengan intersected
            if ($intersected->time_event_type_id == $wfh->time_event_type_id) {
                Session::flash("flash_notification", [
                    "level" => "danger",
                    "message" => $msg
                    . "sudah pernah diajukan sebelumnya (ID " . $intersected->id . ": "
                    . $intersected->check_date->format('Y-m-d') . " " . $intersected->check_time ." - "
                    . $intersected->timeEventType->description . ").",
                ]);
                return false;
            }
        }
    }

    public function created(Wfh $wfh)
    {
        // karyawan yang membuat time_event
        $employee = $wfh->employee;
        
        // mencari atasan dari karyawan yang mengajukan time_events
        $supeintendentBoss = $employee->minSptBossWithDelegation();

        // mencari direktur dari karyawan yang mengajukan time_event
        $director = $employee->director();

        // buat record untuk time_event approval
        $wfhApproval = new WfhApproval();

        // foreign key pada time_event approval
        $wfhApproval->wfh_id = $wfh->id;

        // NEED TO IMPLEMENT FLOW STAGE
        // mengambil status dari firststatus
        $wfhApproval->sequence = 1;
        $wfhApproval->status_id = Status::firstStatus()->id;

        // JIKA karyawan tersebut mempunyai atasan langsung
        // maka simpan data atasan sebagai time_event approval
        // JIKA TIDAK mempunyai atasan langsung
        // maka time_event approval dibuat seolah-olah sudah disetujui
        // contoh: karyawan yang atasannya langsung direktur
        // atau deputi (UTOMO NUGROHO) 

        if ($wfh->time_event_type_id == 3 || $wfh->time_event_type_id == 4) {
            if ($employee->is_a_transfer_knowledge || !$supeintendentBoss) {
                
                //approved by admin
                $wfh->stage_id = 2;

                // bypass regno menggunakan admin  dan sequence
                $admin = Role::retrieveAdmin();
                $wfhApproval->regno = $admin->personnel_no;
                $wfhApproval->sequence = 1;

                // menyimpan data persetujuan
                $wfhApproval->save();

                // mengubah status menjadi approved
                $wfhApproval->status_id = Status::ApproveStatus()->id;
                $wfhApproval->text = 'Disetujui oleh Admin';

                // menyimpan perubahan agar mentrigger observer
                $wfhApproval->save();
            } else {
                //stage waiting approved
                $wfh->stage_id = 1;
                // menyimpan personnel_no dari closest boss
                $wfhApproval->regno = $supeintendentBoss->personnel_no;

                // menyimpan data persetujuan
                $wfhApproval->save();
            }

            $wfh->save();
        }

        if($wfh->time_event_type_id == 3){
            $msg = "Check in Success";
            $key = "check-in";
        }else{
            $msg = "Check out Success";
            $key = "success";
        }
        session()->flash($key,$msg);
    }

    public function deleting(Wfh $wfh)
    {
        $approvals = $wfh->WfhApproval;
        
        // hapus semua approval terkait Wfh
        foreach ($approvals as $approval)
            $approval->delete();

        // sistem mengirim notifikasi
        $to = $wfh->user;
        if ($to->hasValidEmail)
            $to->notify(new WfhDeletedMessage($wfh));
    }
}
