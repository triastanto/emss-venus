<?php

namespace App\Observers;

use App\Message;
use App\Models\WfhApproval;
use App\Models\Employee;
use App\Notifications\WfhApprovalMessage;
use App\Notifications\WfhApprovalCreatedMessage;

class WfhApprovalObserver
{
  public function created(WfhApproval $wfhApproval)
  {
      // to adalah karyawan yang mengajukan
      $to = Employee::findByPersonnel($wfhApproval->regno)
          ->first()
          ->user;

      // sistem mengirim email notifikasi
      if ($to->hasValidEmail)
        $to->notify(new WfhApprovalCreatedMessage($wfhApproval));
  }

  public function updated(WfhApproval $wfhApproval)
  {
      // $flow_id  = config('emss.flows.time_events');
      // $flow_stage = FlowStage::nextSequence($flow_id);

      // mencari data wfh sesuai dengan relatioship
      $wfh = $wfhApproval->wfh()->first();

      // from adalah dari atasan
      $from = $wfhApproval->user()->first();

      // to adalah karyawan yang mengajukan
      $to = $wfh->user()->first();

      // menyimpan catatan pengiriman pesan
      $message = new Message;

      // apakah data wfh sudah disetujui
      if ($wfhApproval->isApproved) {

          // NEED TO IMPLEMENT FLOW STAGE (send to SAP)
          $wfh->stage_id = 2;

          // message history
          $messageAttribute = sprintf('Work from Home approved from %s to %s',
              $from->personnelNoWithName, $to->personnelNoWithName);
      } else {

          // NEED TO IMPLEMENT FLOW STAGE (denied)
          $wfh->stage_id = 5;

          // message history
          $messageAttribute = sprintf('Work from Home rejected from %s to %s',
              $from->personnelNoWithName, $to->personnelNoWithName);
      }

      // simpan data message history lainnya
      $message->setAttribute('from', $from->id);
      $message->setAttribute('to', $to->id);
      $message->setAttribute('message', $messageAttribute);

      // simpan message history
      $message->save();

      // update data wfh
      $wfh->save();

      // sistem mengirim email notifikasi dari atasan ke
      // karyawan yang mengajukan
      if ($to->hasValidEmail)
        $to->notify(new WfhApprovalMessage($from, $wfhApproval));
  }
}
