<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;
use App\Models\WfhApproval;

class WfhApprovalMessage extends Notification implements ShouldQueue
{
    use Queueable;
    public $tries = 3; // Max tries
    public $timeout = 15; // Timeout seconds
    public $approved;
    public $fromUser;
    public $wfhApproval;
    
    public function __construct(User $user, WfhApproval $wfhApproval)
    {
        $this->fromUser = $user;
        $this->approved = $wfhApproval->isApproved;
        // lazy eager loading
        $this->wfhApproval = $wfhApproval->load('wfh.stage');
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        // judul email
        $subjectText = ($this->approved) ? 'Pengajuan work form home Anda telah disetujui' :
            'Pengajuan work form home Anda telah ditolak';
        $subject = sprintf('%s: %s oleh %s!', 'Work from Home', $subjectText, $this->fromUser->name );
        
        // kalimat pembuka
        $greetingText = ($this->approved) ? 'Selamat' : 'Mohon maaf';
        $greeting = sprintf('%s %s!', $greetingText, $notifiable->name);

        // catatan persetujuan
        $approvalNotes = sprintf('Catatan: %s', $this->wfhApproval->text);

        // catatan tahapan persetujuan
        $currentStageText = sprintf('Tahapan pengajuan: %s', $this->wfhApproval->wfh->stage->description);

        // link untuk melihat tidak slashexit
        $url = route('e-absence.index');
        
        // mengirim email
        return (new MailMessage)
                    ->subject($subject)
                    ->greeting($greeting)
                    ->line($subjectText)
                    ->line('Jenis: ' . $this->wfhApproval->wfh->timeEventType->description)
                    ->line($approvalNotes)
                    ->line($currentStageText)
                    ->action('Lihat Work form Home', $url);
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
