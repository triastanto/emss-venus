<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\WfhApproval;

class WfhApprovalCreatedMessage extends Notification  implements ShouldQueue
{
    use Queueable;
    protected $wfhApproval, $wfh;
    public $tries = 5;
    
    public function __construct(WfhApproval $wfhApproval)
    {
        $this->WfhApproval = $wfhApproval;
        $this->wfh = $wfhApproval->wfh;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $wfhApprovalText = "Work from Home";
        
        // judul email
        $subject = sprintf('Pengajuan %s oleh %s', 
            $wfhApprovalText,
            $this->wfh->employee->personnelNoWithName
        );

        // kalimat pembuka
        $greeting = sprintf('Telah dibuat pengajuan %s dengan rincian:',
            $wfhApprovalText);

        // mengirim email
        return (new MailMessage)
                    ->subject($subject)
                    ->greeting($greeting)
                    ->line('Karyawan: ' . $this->wfh->employee->personnelNoWithName)
                    ->line('Jenis: ' . $this->wfh->timeEventType->description)
                    ->line('Tanggal: ' . $this->wfh->formatted_check_date)
                    ->line('Jam: ' . $this->wfh->check_time)
                    ->action('Lihat pengajuan', route('e-absence.index'));
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
