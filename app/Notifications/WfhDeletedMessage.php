<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Wfh;

class WfhDeletedMessage extends Notification implements ShouldQueue
{
    use Queueable;
    public $wfh;
    public $tries = 5;
    
    public function __construct(Wfh $wfh)
    {
        $this->wfh = $wfh;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        // judul email
        $subjectText = 'Pengajuan Wfh Anda telah dihapus oleh Admin';
        $moduleText = 'Worfk Form Home';
        $subject = sprintf('%s: %s', $moduleText, $subjectText );
        
        // kalimat pembuka
        $greetingText = 'Informasi';
        $greeting = sprintf('%s %s!', $greetingText, $notifiable->name);

        // mengirim email
        return (new MailMessage)
                    ->subject($subject)
                    ->greeting($greeting)
                    ->line($this->wfh->check_date)
                    ->line($this->wfh->check_time)
                    ->line($this->wfh->timeEventType->description);
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
