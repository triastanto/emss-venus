<!-- Trigger the modal detail with a button -->
<button type="button" class="btn btn-info btn-sm m-1" data-toggle="modal" data-target="#modalDetail{{$ski->id}}">Detail</button>

<!-- Modal detail-->
<div id="modalDetail{{$ski->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 85%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SKI Detail : {{$ski->plain_id}}</h4>
            </div>
            <div class="modal-body">
                @include('ski.show')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@if ($ski->skiApproval->where('sequence', 1)->first()->status_id == 1)

    @include('ski._editPerilaku')
    @include('ski._editKinerja')

    <!-- Trigger the modal hapus with a button -->
    <button type="button" class="btn btn-danger btn-sm m-1" data-toggle="modal" data-target="#modalHapus{{$ski->id}}">Hapus</button>

    <!-- Modal hapus-->
    <div id="modalHapus{{$ski->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Hapus : {{$ski->plain_id}}</h4>
                </div>
                <div class="modal-body text-center">
                    <form action="{{ route('ski.destroy',$ski->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button class="btn btn-md btn-default" type="button" data-dismiss="modal">TIDAK</button>
                        <button class="btn btn-md btn-danger" type="submit">YA</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endif