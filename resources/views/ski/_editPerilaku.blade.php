<!-- Trigger the modal Edit with a button -->
<button type="button" class="btn btn-success btn-sm m-1" data-toggle="modal" data-target="#modalPerilakuEdit{{$ski->id}}">Edit Perilaku</button>

<!-- Modal Edit-->
<div id="modalPerilakuEdit{{$ski->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 85%">

        <form action="{{ route('ski.update.perilaku',$ski->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Perilaku : {{$ski->plain_id}}</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success fade in">
                        <div class="row">
                            <div class="col-md-3">
                                <p>Di bawah TARGET</p>
                                <p>SKOR (0 - 4.5)</p>
                                <p>Pencapaian 0% - < 70%</p> 
                            </div> 
                            <div class="col-md-3">
                                <p>Mendekati TARGET</p>
                                <p>SKOR 4.6 - 6.5</p>
                                <P>Pencapaian 70% - < 90% </p> 
                            </div> 
                            <div class="col-md-3">
                                <p>Memenuhi TARGET</p>
                                <p>SKOR 6.6 - 8.0</p>
                            </div>
                            <div class="col-md-3">
                                <p>Melebihi TARGET</p>
                                <p>SKOR 8.1-10</P>
                            </div>
                        </div>
                    </div>
                    <table class="table-responsive" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%">NO</th>
                                <th class="text-center" style="width: 30%">Sasaran Kerja</th>
                                <th class="text-center" style="width: 10%">Kode</th>
                                <th class="text-center" style="width: 25%">Ukuran Prestasi Kerja</th>
                                <th class="text-center" style="width: 6%">Bobot</th>
                                @if ($golongan[0] == 'A')
                                <th class="text-center" style="width: 6%">Skor</th>
                                <th class="text-center" style="width: 8%">Nilai</th>
                                @endif

                            </tr>
                        </thead>
                        <tbody id="tbodyPerilaku">
                            @php($u = 0)
                            @php($sum_bobot = 0)
                            @php($sum_nilai = 0)
                            @php($regno = $ski->skiApproval->where('sequence', 1)->first()->regno)
                            @php($month = $ski->month)
                            @php($year = $ski->year)

                            @foreach($ski->skiDetail->where('klp','Perilaku') as $key => $perilaku)
                                <tr>
                                    <td class="text-center p-1">{{ $u+1 }}</td>
                                    <td class="p-1">
                                        <input type="hidden" name="klpp[]" id="klpp{{$ski->id}}{{$u}}" style="width: 100%" value="Perilaku">
                                        <input type="text" class="form-control" name="sasaranp[]" autocomplete="off" style="width: 100%" value="{{ $perilaku->sasaran }}">
                                    </td>
                                    <td class="p-1">
                                        <input type="text" class="form-control" id="kodep{{$ski->id}}{{$u}}" name="kodep[]" autocomplete="off" style="width: 100%" value="{{ $perilaku->kode }}">
                                    </td>
                                    <td class="p-1">
                                        <input type="text" class="form-control" id="ukuranp{{$ski->id}}{{$u}}" name="ukuranp[]" autocomplete="off" style="width: 100%" value="{{ $perilaku->ukuran }}">
                                    </td>
                                    <td class="p-1">
                                        <input type="text" class="form-control" name="bobotp[]" autocomplete="off" 
                                            id="bobotp{{$ski->id}}{{$u}}" value="{{ $perilaku->bobot }}" style="width: 100%; text-align: right"
                                            onkeyup="setNilaiPerilaku({{$ski->id}}{{$u}})" readonly>
                                    </td>
                                    @if ($golongan[0] == 'A')
                                    <td class="p-1">
                                        <input type="text" class="form-control" name="skorp[]" id="skorp{{$ski->id}}{{$u}}" autocomplete="off"
                                            style="width: 100%; text-align: right" value="{{ $perilaku->skor }}"
                                            onkeyup="setNilaiPerilaku({{$ski->id}}{{$u}});checkSkorPerilaku(this.value, '{{$ski->id}}{{$u}}')">
                                    </td>
                                    <td class="p-1">
                                        <input type="text" class="form-control" name="nilaip[]" value="{{ $perilaku->nilai }}"
                                            id="nilaip{{$ski->id}}{{$u}}" style="width: 100%; text-align: right" readonly>
                                    </td>
                                    @endif
                                </tr>
                                @php($u++)
                                @php($sum_bobot += $perilaku->bobot)
                                @php($sum_nilai += $perilaku->nilai)
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right">Total: </td>
                                <td class="text-right p-1">
                                    <input class="text-right form-control" type="text" value="{{$sum_bobot}}" id="sum_perilaku1{{$ski->id}}" readonly>
                                </td>
                                @if ($golongan[0] == 'A')
                                <td class="text-right p-1"></td>
                                <td class="text-right p-1">
                                    <input class="text-right form-control" type="text" value="{{$sum_nilai}}" id="sum_nilai_perilaku1{{$ski->id}}" readonly>
                                </td>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                    <input type="hidden" id="idp{{$ski->id}}" value="{{$u}}">
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"> Kirim </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>

        </form>

    </div>
</div>