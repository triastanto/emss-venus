<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 85%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sasaran Kerja</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success fade in">
                    <div class="row">
                        <div class="col-md-3">
                            <p>Di bawah TARGET</p>
                            <p>SKOR (0 - 4.5)</p>
                            <p>Pencapaian 0% - < 70%</p> 
                        </div> 
                        <div class="col-md-3">
                            <p>Mendekati TARGET</p>
                            <p>SKOR 4.6 - 6.5</p>
                            <P>Pencapaian 70% - < 90% </p> 
                        </div> 
                        <div class="col-md-3">
                            <p>Memenuhi TARGET</p>
                            <p>SKOR 6.6 - 8.0</p>
                        </div>
                        <div class="col-md-3">
                            <p>Melebihi TARGET</p>
                            <p>SKOR 8.1-10</P>
                        </div>
                    </div>
                </div>
                <table class="table-responsive" style="width: 100%">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%">NO</th>
                            {{-- <th class="text-center" style="width: 10%">KLP</th> --}}
                            <th class="text-center" style="width: 30%">Sasaran Kerja</th>
                            <th class="text-center" style="width: 10%">Kode</th>
                            <th class="text-center" style="width: 25%">Ukuran Prestasi Kerja</th>
                            <th class="text-center" style="width: 6%">Bobot</th>
                            @if ($golongan[0] == 'A')
                            <th class="text-center" style="width: 6%">Skor</th>
                            <th class="text-center" style="width: 8%">Nilai</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody id="tbody">
                        @for ($i = 0; $i < 15; $i++) @php($required='' ) 
                            @if ($i==0) @php($required='' ) @endif 
                            <tr>
                                <td class="text-center p-1">
                                    {{$i+1}}
                                    <input type="hidden" name="klp[]" id="klp{{ $i }}" style="width: 100%" value="Kinerja">
                                </td>
                                <td class="p-1">
                                    <input type="text" class="form-control" autocomplete="off" {{ $required }} name="sasaran[]" id="sasaran{{$i}}"
                                        style="width: 100%" value="{{ old('sasaran')[$i] }}">
                                </td>
                                <td class="p-1">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="kode{{$i}}" name="kode[]" autocomplete="off" style="width: 100%" value="{{ old('kode')[$i] }}">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="button" onclick="findCode({{$i}},'Kinerja')">
                                            <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td class="p-1"><input type="text" class="form-control" autocomplete="off" id="ukuran{{$i}}" name="ukuran[]" style="width: 100%" value="{{ old('ukuran')[$i] }}"></td>
                                <td class="p-1">
                                    <input type="text" {{ $required }} class="form-control" autocomplete="off" name="bobot[]" id="bobot{{$i}}" value="{{ old('bobot')[$i] }}"
                                        style="width: 100%; text-align: right" onkeyup="setNilai({{$i}})">
                                </td>
                                @if ($golongan[0] == 'A')
                                <td class="p-1">
                                    <input type="text" class="form-control" name="skor[]" autocomplete="off" id="skor{{$i}}" 
                                        style="width: 100%; text-align: right" value="{{ old('skor')[$i] }}" 
                                        onkeyup="setNilai({{$i}}); checkSkor(this.value, '{{$i}}')">
                                </td>
                                <td class="p-1">
                                    <input type="text" class="form-control" name="nilai[]" autocomplete="off"  value="{{ old('nilai')[$i] }}" 
                                        id="nilai{{$i}}" style="width: 100%; text-align: right" readonly>
                                </td>
                                @endif
                            </tr>
                            @endfor
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right p-1">Total: </td>
                            <td class="text-right p-1"><input type="text" class="form-control" id="sum_kinerja1" readonly></td>
                            @if ($golongan[0] == 'A')
                            <td class="text-right p-1"></td>
                            <td class="text-right p-1"><input type="text" class="form-control" id="sum_nilai_kinerja1" readonly></td>
                            @endif
                        </tr>
                    </tfoot>
                </table>
                <input type="hidden" id="id" value="{{$i}}">
                <input type="hidden" id="sum_kinerja">
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <span id="bobot_kinerja"></span>
                </div>

                <button type="submit" class="btn btn-primary hidden" id="kirim">
                    Kirim
                </button>

                <button type="button" class="btn btn-warning" id="tambah_kolom" onclick="add_column({{$i}})">
                    Tambah Kolom
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>