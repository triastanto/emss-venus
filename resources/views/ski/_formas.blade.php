<div class="col-lg-12">
  <!-- begin personnel_no field -->
  <div class="form-group p-l-5 p-r-5 {{ $errors->has('personnel_no') ? ' has-error' : '' }}">
    {!! Form::label('personnel_no', 'Karyawan') !!}
    {!! Form::select( 'personnel_no', [], null,
      [
        'class'=>'form-control sub-selectize',
        'id'=>'personnel_no',
        'data-parsley-required' => 'true',
        'placeholder' => 'Silahkan pilih karyawan',
        'required' => true
      ])
    !!}
    {!! $errors->first('personnel_no', '<p class="help-block">:message</p>') !!}
  </div>
  <!-- end personnel_no field -->
  
  <div class="form-group {{ $errors->has('periode') ? ' has-error' : '' }}">
    <div class="col-xs-6 p-l-5 p-r-5">
      <label for="">Pilih Bulan</label>
      <select name="bulan" id="bulan" class="form-control">
        <option value="06" selected>Juni</option>
        <option value="12">Desember</option>
      </select>
    </div>
    <div class="col-xs-6 p-l-5 p-r-5">
      <label for="">Pilih Tahun</label>
      <select name="tahun" id="tahun" class="form-control">
        @for ($i = 0; $i < 10; $i++) @if ($i==0) <option value="{{date('Y') - $i}}" selected>{{date('Y') - $i}}</option>
          @else
          <option value="{{date('Y') - $i}}">{{date('Y') - $i}}</option>
          @endif
          @endfor
      </select>
    </div>
  </div>
    
  <!-- begin superintendent_approver field -->
  <!-- This field is not sent via form -->
  <div class="p-l-5 p-r-5 form-group{{ $errors->has('superintendent_approver') ? ' has-error' : '' }}">
    {!! Form::label('superintendent_approver', 'Superintendent') !!}
    <select class="form-control superintendent-selectize" name="superintendent" id="superintendent">
      <option value="" selected>Pilih Superintendent</option>
    </select>
  </div>
  <!-- end superintendent_approver field -->
  
  <!-- begin manager_approver field -->
  <!-- This field is not sent via form -->
  <div class="p-l-5 p-r-5 form-group{{ $errors->has('manager_approver') ? ' has-error' : '' }}">
    {!! Form::label('manager_approver', 'Manager') !!}
    <select class="form-control manager-selectize" name="manager">
      <option value="" selected>Pilih Manager</option>
    </select>
  </div>
  
  <div class="p-l-5 p-r-5 form-group">
    <button id="btn-perilaku"
      type="button" 
      class="btn btn-warning" 
      data-backdrop="static" 
      data-toggle="modal" 
      data-target="#modalPrilaku"
      style="display: none"
      onclick="return ceking(1)">
      Input Perilaku
    </button>

    <button id="btn-sasaran"
      type="button" 
      class="btn btn-warning" 
      data-backdrop="static" 
      data-toggle="modal" 
      data-target="#myModal"
      style="display: none"
      onclick="return ceking(2)">
      Input Kinerja
    </button>

    <button type="button" 
      class="btn btn-primary pull-right" 
      data-toggle="modal" 
      data-target="#exampleModal">
      Job Description
    </button>
  </div>
</div>
<!-- Modal perilaku -->
@include('ski._modalPerilaku')

<!-- Modal -->
@include('ski._modalKinerja')

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-body" style="height: 95vh">
      <iframe src="https://sso.krakatausteel.com/hci/dokumen/jobdesc/{{$abbre}}.pdf" width="100%" height="100%"></iframe>
    </div>
  </div>
</div>
</div>