<div id="modalPrilaku" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 85%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Perilaku</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success fade in">
                    <div class="row">
                        <div class="col-md-3">
                            <p>Di bawah TARGET</p>
                            <p>SKOR (0 - 4.5)</p>
                            <p>Pencapaian 0% - < 70%</p> </div> <div class="col-md-3">
                                    <p>Mendekati TARGET</p>
                                    <p>SKOR 4.6 - 6.5</p>
                                    <P>Pencapaian 70% - < 90% </p> </div> <div class="col-md-3">
                                            <p>Memenuhi TARGET</p>
                                            <p>SKOR 6.6 - 8.0</p>
                        </div>
                        <div class="col-md-3">
                            <p>Melebihi TARGET</p>
                            <p>SKOR 8.1-10</P>
                        </div>
                    </div>
                </div>
                <table class="table-responsive" style="width: 100%">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%">NO</th>
                            <th class="text-center" style="width: 30%">Sasaran Kerja</th>
                            <th class="text-center" style="width: 10%">Kode</th>
                            <th class="text-center" style="width: 25%">Ukuran Prestasi Kerja</th>
                            <th class="text-center" style="width: 6%">Bobot</th>
                            @if ($golongan[0] == 'A')
                            <th class="text-center" style="width: 6%">Skor</th>
                            <th class="text-center" style="width: 8%">Nilai</th>
                            @endif

                        </tr>
                    </thead>
                    <tbody id="tbodyPerilaku">
                        @php($u = 1)
                        @foreach($perilakus as $key => $perilaku)
                        <tr>
                            <td class="text-center p-1">{{ $u++ }}</td>
                            <td class="p-1">
                                <input type="hidden" name="klpp[]" id="klpp{{ $key }}" style="width: 100%" value="Perilaku">
                                <input type="text" class="form-control" name="sasaranp[]" style="width: 100%" value="{{ $perilaku->name }}">
                            </td>
                            <td class="p-1">
                                <input type="text" class="form-control" id="kodep{{$key}}" name="kodep[]" autocomplete="off" style="width: 100%" value="{{ old('kodep')[$key] }}">
                            </td>
                            <td class="p-1">
                                <input type="text" class="form-control" id="ukuranp{{$key}}" name="ukuranp[]" autocomplete="off" style="width: 100%" value="{{ old('ukuranp')[$key] }}">
                            </td>
                            <td class="p-1">
                                <input type="text" class="form-control" name="bobotp[]"
                                    id="bobotp{{$key}}" value="10" style="width: 100%; text-align: right"
                                    onkeyup="setNilaiPerilaku({{$key}})" readonly>
                            </td>
                            @if ($golongan[0] == 'A')
                            <td class="p-1">
                                <input type="text" class="form-control" name="skorp[]" id="skorp{{$key}}"
                                    style="width: 100%; text-align: right" autocomplete="off"
                                    onkeyup="setNilaiPerilaku({{$key}});checkSkorPerilaku(this.value, '{{$key}}')">
                            </td>
                            <td class="p-1">
                                <input type="text" class="form-control" name="nilaip[]" id="nilaip{{$key}}" 
                                    style="width: 100%; text-align: right" readonly>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right">Total: </td>
                            <td class="text-right p-1">
                                <input class="text-right form-control" type="text" value="100" id="sum_perilaku1" readonly>
                            </td>
                            @if ($golongan[0] == 'A')
                            <td class="text-right p-1"></td>
                            <td class="text-right p-1">
                                <input class="text-right form-control" type="text" id="sum_nilai_perilaku1" readonly>
                            </td>
                            @endif
                        </tr>
                    </tfoot>
                </table>
                <input type="hidden" id="idp" value="{{$key+1}}">
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <span id="bobot_perilaku"></span>
                </div>

                <button type="submit" class="btn btn-primary" id="kirimPerilaku">
                    Kirim
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>