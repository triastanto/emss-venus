<!-- Trigger the modal Edit with a button -->
<button type="button" class="btn btn-success btn-sm m-1" data-toggle="modal" data-target="#modalKinerjaEdit{{$ski->id}}">Edit Kinerja</button>

<!-- Modal Edit-->
<div id="modalKinerjaEdit{{$ski->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 85%">

        <form action="{{ route('ski.update.kinerja',$ski->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Kinerja : {{$ski->plain_id}}</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success fade in">
                        <div class="row">
                            <div class="col-md-3">
                                <p>Di bawah TARGET</p>
                                <p>SKOR (0 - 4.5)</p>
                                <p>Pencapaian 0% - < 70%</p> 
                            </div> 
                            <div class="col-md-3">
                                <p>Mendekati TARGET</p>
                                <p>SKOR 4.6 - 6.5</p>
                                <P>Pencapaian 70% - < 90% </p> 
                            </div> 
                            <div class="col-md-3">
                                <p>Memenuhi TARGET</p>
                                <p>SKOR 6.6 - 8.0</p>
                            </div>
                            <div class="col-md-3">
                                <p>Melebihi TARGET</p>
                                <p>SKOR 8.1-10</P>
                            </div>
                        </div>
                    </div>
                    <table class="table-responsive" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%">NO</th>
                                {{-- <th class="text-center" style="width: 10%">KLP</th> --}}
                                <th class="text-center" style="width: 30%">Sasaran Kerja</th>
                                <th class="text-center" style="width: 10%">Kode</th>
                                <th class="text-center" style="width: 25%">Ukuran Prestasi Kerja</th>
                                <th class="text-center" style="width: 6%">Bobot</th>
                                @if ($golongan[0] == 'A')
                                <th class="text-center" style="width: 6%">Skor</th>
                                <th class="text-center" style="width: 8%">Nilai</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody id="tbody{{$ski->id}}">
                            @php($i = 0)
                            @php($sum_bobot = 0)
                            @php($sum_nilai = 0)
                            @php($regno = $ski->skiApproval->where('sequence', 1)->first()->regno)
                            @php($month = $ski->month)
                            @php($year = $ski->year)

                            @foreach($ski->skiDetail->where('klp','Kinerja') as $key => $kinerja)
                                @php($required='' ) 
                                @if ($key==0) 
                                    @php($required='' ) 
                                @endif 
                                <tr>
                                    <td class="text-center p-1">
                                        {{$i+1}}
                                        <input type="hidden" name="klp[]" id="klp{{$ski->id}}_{{$i}}" style="width: 100%" value="Kinerja">
                                    </td>
                                    <td class="p-1">
                                        <input type="text" 
                                            class="form-control" 
                                            {{ $required }} 
                                            name="sasaran[]" 
                                            id="sasaran{{$ski->id}}_{{$i}}"
                                            style="width: 100%" 
                                            value="{{$kinerja->sasaran}}">
                                    </td>
                                    <td class="p-1">
                                        <div class="input-group">
                                            <input type="text" 
                                                class="form-control" 
                                                id="kode{{$ski->id}}_{{$i}}" 
                                                name="kode[]" 
                                                autocomplete="off" 
                                                style="width: 100%" 
                                                value="{{$kinerja->kode}}">

                                            <div class="input-group-btn">
                                                <button class="btn btn-default" 
                                                    type="button" 
                                                    onclick="findCode({{$ski->id}}+'_'+{{$i}},'Kinerja','{{$regno}}','{{$month}}','{{$year}}')">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="p-1">
                                        <input type="text" 
                                            class="form-control" 
                                            id="ukuran{{$ski->id}}_{{$i}}" 
                                            name="ukuran[]" 
                                            style="width: 100%" 
                                            value="{{$kinerja->ukuran}}">
                                    </td>
                                    <td class="p-1">
                                        <input type="text" 
                                            {{ $required }} 
                                            class="form-control" 
                                            name="bobot[]" 
                                            id="bobot{{$ski->id}}_{{$i}}" 
                                            value="{{$kinerja->bobot}}" 
                                            style="width: 100%; text-align: right" 
                                            onkeyup="setNilai({{$ski->id}}+'_'+{{$i}})"
                                            autocomplete="off">
                                    </td>
                                    @if ($golongan[0] == 'A')
                                        <td class="p-1">
                                            <input type="text" 
                                                class="form-control" 
                                                name="skor[]" 
                                                id="skor{{$ski->id}}_{{$i}}" 
                                                style="width: 100%; text-align: right" 
                                                onkeyup="setNilai({{$ski->id}}+'_'+{{$i}});
                                                checkSkor(this.value, '{{$ski->id}}_{{$i}}')" 
                                                value="{{$kinerja->skor}}">
                                        </td>
                                        <td class="p-1">
                                            <input type="text" 
                                                class="form-control" 
                                                name="nilai[]" 
                                                id="nilai{{$ski->id}}_{{$i}}" 
                                                style="width: 100%; text-align: right" 
                                                readonly 
                                                value="{{$kinerja->nilai}}">
                                        </td>
                                    @endif
                                </tr>
                                @php($i++)
                                @php($sum_bobot += $kinerja->bobot)
                                @php($sum_nilai += $kinerja->nilai)
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right">Total: </td>
                                <td class="text-right p-1">
                                    <input type="text" 
                                        class="form-control text-right" 
                                        id="sum_kinerja1{{$ski->id}}" 
                                        value="{{$sum_bobot}}" 
                                        readonly>
                                </td>
                                @if ($golongan[0] == 'A')
                                    <td class="text-right p-1"></td>
                                    <td class="text-right p-1">
                                        <input type="text" 
                                            class="form-control text-right" 
                                            id="sum_nilai_kinerja1{{$ski->id}}" 
                                            value="{{$sum_nilai}}" 
                                            readonly>
                                    </td>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                    <input type="hidden" id="id{{$ski->id}}" value="{{$i}}">
                    <input type="hidden" id="sum_kinerja{{$ski->id}}">
                </div>
                <div class="modal-footer">
                    <div class="pull-left"> <span id="bobot_kinerja{{$ski->id}}"></span> </div>
                    <button type="submit" class="btn btn-primary {{ $sum_bobot == '100' ? '' : 'hidden' }} " id="kirim{{$ski->id}}"> Kirim</button>
                    <button type="button" class="btn btn-warning" id="tambah_kolom{{$ski->id}}" onclick="add_column({{$ski->id}},'{{$regno}}','{{$month}}','{{$year}}')"> Tambah Kolom </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>

        </form>

    </div>
</div>