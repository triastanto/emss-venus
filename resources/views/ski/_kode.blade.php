<!DOCTYPE html>
<head>
    <link href={{ url("/plugins/bootstrap/css/bootstrap.min.css") }} rel="stylesheet" />
</head>
<html>
<body>
    @if ($ski)
        <table class="table table-striped table-bordered">
            <tr>
                <th colspan="4" class="text-center"> {{ $klp }} </th>
            </tr>
            <tr>
                <th>Kode</th>
                <th>Sasaran Kerja</th>
                <th>Ukuran</th>
                <th>Aksi</th>
            </tr>
            @foreach ($ski->skiDetail as $item)
            @if ($item->kode && $item->klp == $klp)
                <tr>
                    <td>{{$item->kode}}</td>
                    <td>{{$item->sasaran}}</td>
                    <td>{{$item->ukuran}}</td>
                    <td>
                        <button class="btn btn-sm btn-primary" onclick="copyFunc('{{$item->kode}}','{{$item->ukuran}}','{{$item->sasaran}}')">
                            Pilih
                        </button>
                    </td>
                </tr> 
            @endif
            @endforeach
        </table>
    @endif
    Tutup dalam : <span id="count">30</span> detik
    <script src={{ url("/plugins/jquery/jquery-1.9.1.min.js") }}></script>
    <script src={{ url("/plugins/bootstrap/js/bootstrap.min.js") }}></script>
    <script>
        function copyFunc(kode, ukuran, sasaran) {
            if (window.opener != null && !window.opener.closed) {
                if ('{{$klp}}' == 'Perilaku') {
                    window.opener.$('#kodep{{ $key }}').val(kode);
                    window.opener.$('#ukuranp{{ $key }}').val(ukuran);
                }else{
                    window.opener.$('#kode{{ $key }}').val(kode);
                    window.opener.$('#ukuran{{ $key }}').val(ukuran);
                    window.opener.$('#sasaran{{ $key }}').val(sasaran);
                }
            }
            window.close();
        }

        setInterval(function(){
            var count = Number($('#count').html());
            if (count == 0) {
                window.close();
            }
            $('#count').html(count-1);
        }, 1000);

    </script>
</body>

</html>