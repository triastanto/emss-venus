@extends('layouts.app') 

@section('content')
<!-- begin #page-container -->
@component('layouts.employee._page-container', ['page_header' => 'Sasaran Kinerja Individu'])
<div class="panel panel-prussian">
  <div class="panel-heading">
    <h4 class="panel-title">{{ $lembur }}</h4>
  </div>
  <div class="alert alert-success fade in">
     <i class="fa fa-book pull-left"></i>
     <p>Untuk pertanyaan & petunjuk penggunaan hubungi <b>Divisi Human Capital Integration & Adm.</b> telepon <b>72163</b> </p>
     <br>         
    </div>
  @include('layouts._flash')
  <div class="panel-body">
    <p> 
      <a class="btn btn-primary" href="{{ route('ski.create') }}">Tambah</a> 
     <select name="type" id="type" class="form-control" style="width: 20%; display: inline">
       <option value="">.:: All Type ::.</option>
       <option value="1">Self</option>
       <option value="2">Subordinates</option>
     </select>
    </p>
    <div class="table-responsive">
      {!! $html->table(['class'=>'table table-striped', 'width' => '100%']) !!}
    </div>
  </div>
</div>
<div class="modal fade" id="modal-dialog">
  <div class="modal-dialog modal-lg" style="width: 85%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Sasaran Kinerja Individu (ID: <span id="title-span"></span>)</h4>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>
@endcomponent
<!-- end page container -->
@endsection

@push('styles')
<!-- DataTables -->
<link href={{ url("/plugins/DataTables/css/jquery.dataTables.min.css") }} rel="stylesheet" />
<link href={{ url("/plugins/DataTables/Responsive/css/responsive.dataTables.min.css") }} rel="stylesheet" />
<!-- Pace -->    
<script src={{ url("/plugins/pace/pace.min.js") }}></script>
@endpush

@push('plugin-scripts')
<!-- DataTables -->
<script src={{ url("/plugins/DataTables/js/jquery.dataTables.min.js") }}></script>
<script src={{ url("/plugins/DataTables/Responsive/js/dataTables.responsive.min.js") }}></script>
<!-- Generated scripts from DataTables -->
{!! $html->scripts() !!}
@endpush

@push('custom-scripts')
<script>
  oTable = $('.table').DataTable();
  $('#type').change(function(){
      var type = $('#type').val();
      oTable.search(type).draw();
  })

  var windowObjectReference = null;
  function findCode(key,klp,regno,month,year) {
    var personnel_no = $('#personnel_no').val();
    var bulan = month;
    var tahun = year;
    var spt = regno;

    if (personnel_no == '') {
      alert('Pilih Karyawan');
    }else{
      if(windowObjectReference !== null) {
        windowObjectReference.close();
      }
      windowObjectReference = window.open(
        "{{ url('') }}/ski/"+spt+"/kode/"+klp+"/"+bulan+"/"+tahun+"/"+key, 
        "_blank", 
        "location=no,toolbar=yes,scrollbars=yes,resizable=yes,top=250,left=250,width=500,height=400"
      );
    }
  }

  function ceking(isi) {
      $('#aksi').val(isi);
      return true;
  }

  function checkSkor(value, iddata) {
    if(value > 10) {
      alert('Nilai tidak boleh lebih dari 10 ');
      $('#skor'+iddata).val(0);
      $('#nilai'+iddata).val(0);
      e.preventDefault();
      return false;
    }
  }
  
  $(document).on("keypress", 'form', function (e) {
      var code = e.keyCode || e.which;
      if (code == 13) {
          e.preventDefault();
          return false;
      }
  });

  function setNilai(id) {     
    var bobot = Number( $('#bobot'+id).val() );
    var skor = Number( $('#skor'+id).val() );
    var sum_perilaku = 0;
    var sum_kinerja = 0;
    var sum_nilai_kinerja1 = 0;
    var sum_nilai_perilaku1 = 0;
    var kinerja_id = String(id).split('_')[0];
    var count = Number( $('#id'+kinerja_id).val() );

    if(!skor) skor = 1;
    
    $('#nilai'+id).val(bobot*skor);

    for (let index = 0; index < count; index++) {
      var klp = $('#klp'+kinerja_id+'_'+index).val();
      var klpp = $('#klpp'+kinerja_id+'_'+index).val();
      var bobot =  Number( $('#bobot'+kinerja_id+'_'+index).val() );
      var skor =  Number( $('#skor'+kinerja_id+'_'+index).val() );
      if(klp == 'Kinerja'){
        sum_kinerja += bobot;
        $('#sum_kinerja1'+kinerja_id).val(sum_kinerja);
        sum_nilai_kinerja1 += bobot*skor;
        $('#sum_nilai_kinerja1'+kinerja_id).val(sum_nilai_kinerja1);
      }
      
    }

    var msg_kinerja = '';
    if(sum_kinerja < 100 && sum_kinerja !== 0){
      msg_kinerja = '<div class="alert alert-warning alert-dismissible">'+
      '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Bobot Kinerja Kurang dari 100.</strong> '+
      '</div>';
    }
    if(sum_kinerja > 100 && sum_kinerja !== 0){
      msg_kinerja = '<div class="alert alert-warning alert-dismissible">'+
      '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
      '<strong>Bobot Kinerja Lebih dari 100.</strong> '+
      '</div>';
    }
    $('#bobot_kinerja'+kinerja_id).html(msg_kinerja);
    
    if(sum_kinerja == 100){
      $('#kirim'+kinerja_id).removeClass('hidden');
    }else{
      $('#kirim'+kinerja_id).addClass('hidden');
    }
  }

  function add_column(kinerja_id,regno,month,year) {
    var id = Number( $('#id'+kinerja_id).val() );
    var kolom = '<tr>'+
      '<td class="text-center p-1">'+ (id+1) +
      '<input type="hidden" name="klp[]" id="klp'+kinerja_id+'_'+id+'" style="width: 100%" value="Kinerja">'+
      '</td>'+
      '<td class="p-1">'+
      '<input type="text" class="form-control" name="sasaran[]" style="width: 100%">'+
      '</td>'+

      '<td class="p-1">'+
        '<div class="input-group">'+
          '<input type="text" class="form-control" id="kode'+kinerja_id+'_'+id+'" name="kode[]" autocomplete="off" style="width: 100%">'+
          '<div class="input-group-btn">'+
            '<button class="btn btn-default" type="button" onclick="findCode( '+kinerja_id+'_'+id+',' +'\'Kinerja\''+','+regno+',\''+month+'\','+year+')">'+
            '<i class="glyphicon glyphicon-search"></i>'+
            '</button>'+
          '</div>'+
        '</div>'+
      '</td>'+

      '<td class="p-1"><input type="text" class="form-control" id="ukuran'+kinerja_id+'_'+id+'" name="ukuran[]" style="width: 100%"></td>'+
      '<td class="p-1"> '+
        '<input type="text" '+
          'class="form-control" '+
          'name="bobot[]" '+
          'id="bobot'+kinerja_id+'_'+id+'" '+
          'style="width: 100%; text-align: right"'+
          'onkeyup="setNilai('+kinerja_id+'+\'' + '_' + '\'+'+id+')"'+
          'autocomplete="off" '+
        '> '+
      '</td>'+
      @if ($golongan[0] == 'A')
      '<td class="p-1"> '+
        '<input type="text" class="form-control" '+
          'name="skor[]" '+
          'id="skor'+kinerja_id+'_'+id+'" '+
          'style="width: 100%; text-align: right"'+
          'onkeyup="setNilai('+kinerja_id+'+\'' + '_' + '\'+'+id+')"'+
        '>'+
      '</td>'+
      '<td class="p-1">'+
        '<input type="text" class="form-control" '+
          'name="nilai[]" '+
          'id="nilai'+kinerja_id+'_'+id+'" '+
          'style="width: 100%; text-align: right"'+
          'readonly'+
        '>'+
      '</td>'+
      @endif
      '</tr>';
    $('#tbody'+kinerja_id).append(kolom);
    $('#id'+kinerja_id).val(id+1);
  }
</script>

@include('layouts._modal-detail-script')
@endpush

@push('on-ready-scripts') 
ModalDetailPlugins.init();
@endpush
