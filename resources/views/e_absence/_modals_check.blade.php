@if ($item->wfhCheck->all())

<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalCheck{{$item->id}}">
   medical check-up detail
</button>
<!-- Modal -->
<div class="modal fade" id="myModalCheck{{$item->id}}" role="dialog">
   <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detail</h4>
         </div>
         <div class="modal-body">
            <div class="table-responsive">
               <table class="table table-bordered table-striped table-hover">
                  <thead>
                     <tr>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Gejala</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach ($item->wfhCheck as $item)
                     <tr>
                        <td>{{ $item->keluarga}}</td>
                        <td>{{ $item->keterangan}}</td>
                        <td>{{ $item->gejala}}</td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>

   </div>
</div>

@endif