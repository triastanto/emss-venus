<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Apakah anda dan keluarga anda sehat hari ini?</h4>
         </div>
         <div class="modal-body text-center">
            <button class="btn btn-primary btn-lg m-10" data-dismiss="modal">Ya</button>
            <button class="btn btn-warning btn-lg m-10" id="tidak">Tidak</button>
         </div>
      </div>

   </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
   <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Input Data Karyawan dan Keluarga</h4>
         </div>
         <div class="modal-body" style="overflow-y: scroll; height: 90vh;">
            {!! Form::open([
               'url' => "/e-absence/wfh-check", 
               'method' => 'post'
               ])
           !!}
               <input type="hidden" name="wfh_id" value="{{base64_encode(base64_encode(session('wfh_id')))}}">
               <span class="title" for="keluarga">Pilih Karyawan & Keluarga</span>
               <select name="keluarga[]" id="keluarga" class="form-control m-b-10" required>
                  <option value="">.: Pilih Data :.</option>
                  <option value="{{Auth::user()->name}}">{{Auth::user()->name}}</option>
                  @foreach ($family as $item)
                  <option value="{{$item->FCNAM}}">{{$item->FCNAM}} ({{$item->T591S_STEXT}})</option>
                  @endforeach
               </select>

               <span class="title" for="keterangan">Keterangan Sakit</span>
               <textarea name="keterangan[]" class="form-control  m-b-10" id="keterangan" required cols="30" rows="3"></textarea>

               <span class="title" for="keluarga">Gejala yang dirasakan</span>
               <select name="gejala[]" id="gejala" class="form-control m-b-10" required>
                  <option value="">.: Pilih Data :.</option>
                  <option value="Demam">Demam</option>
                  <option value="Batuk">Batuk</option>
                  <option value="Flu">Flu</option>
                  <option value="Sesak Nafas">Sesak Nafas</option>
                  <option value="Lemas & Nyeri Badan">Lemas & Nyeri Badan</option>
                  <option value="Lainnya">Lainnya</option>
               </select>
               <div id="add"></div>
               <div class="text-center">
                  <button type="button" class="btn btn-primary m-r-5 m-l-5" data-dismiss="modal">Batal</button>
                  <button type="button" class="btn btn-primary m-r-5 m-l-5" id="tambah">Tambah</button>
                  <button type="submit" class="btn btn-primary m-r-5 m-l-5">Kirim</button>
               </div>
            {!! Form::close() !!}
         </div>
      </div>

   </div>
</div>

@push('custom-scripts')
<script>
   $('#tidak').click(function(){
         $('#myModal').modal('hide');
         $('#myModal2').modal({backdrop: 'static', keyboard: false});
      });

      $('#tambah').click(function(){
         let r = '_'+Math.random().toString(36).substring(7);
         var form = 
            '<div id="'+r+'">'+
            '<hr>'+
            '<button class="btn btn-sm pull-right" onclick="hapus(\''+r+'\')">Hapus 👇</button><br>'+

            '<span class="title" for="keluarga">Pilih Karyawan & Keluarga</span>'+
            '<select name="keluarga[]" id="keluarga" class="form-control m-b-10" required>'+
            '<option value="">.: Pilih Data :.</option>'+
            '<option value="{{Auth::user()->name}}">{{Auth::user()->name}}</option>'+
            @foreach ($family as $item)
                  '<option value="{{$item->FCNAM}}">{{$item->FCNAM}} ({{$item->T591S_STEXT}})</option>'+
            @endforeach
            '</select>'+

            '<span class="title" for="keterangan">Keterangan Sakit</span>'+
            '<textarea name="keterangan[]" class="form-control  m-b-10" id="keterangan" required cols="30" rows="3"></textarea>'+

            '<span class="title" for="keluarga">Gejala yang dirasakan</span>'+
            '<select name="gejala[]" id="gejala" class="form-control m-b-10" required>'+
            '<option value="">.: Pilih Data :.</option>'+
            '<option value="Demam">Demam</option>'+
            '<option value="Batuk">Batuk</option>'+
            '<option value="Flu">Flu</option>'+
            '<option value="Sesak Nafas">Sesak Nafas</option>'+
            '<option value="Lemas & Nyeri Badan">Lemas & Nyeri Badan</option>'+
            '<option value="Lainnya">Lainnya</option>'+
            '</select>'+

            '</div>';
         $('#add').append(form);
      });

      function hapus(params) {
         $('#'+params).remove();
      }
</script>
@endpush