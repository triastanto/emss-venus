@extends('layouts.app')

@section('content')
<!-- begin #page-container -->
@component('layouts.employee._page-container', ['page_header' => 'Waktu Kerja'])
<div class="panel panel-prussian">
    <div class="panel-heading">
        <h4 class="panel-title">Absence Work from Home</h4>
    </div>
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
        <strong>Untuk keakuratan lokasi anda, harap membuka aplikasi ini dengan browser smartphone.</strong>
    </div>

    @include('layouts._flash')

    @if (session('check-in'))
    <div class="alert alert-success">
        {{ session('check-in') }}
    </div>
    <!-- Modal -->
    @include('e_absence._modals')
    @elseif(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 text-center">
                <form action="{{route('e-absence.store')}}" method="post" style="display: inline;">
                    {{ csrf_field() }}
                    <input type="hidden" name="time_in" value="{{date('H:i:s')}}">
                    <input type="hidden" name="location" class="location" value="Lokasi tidak terdeteksi">
                    <input type="hidden" name="latitude" class="latitude" value="0">
                    <input type="hidden" name="longitude" class="longitude" value="0">
                    <button type="submit" class="btn btn-success">Check In</button>
                </form>
                &nbsp;
                &nbsp;
                <form action="{{route('e-absence.store')}}" method="post" style="display: inline;">
                    {{ csrf_field() }}
                    <input type="hidden" name="time_out" value="{{date('H:i:s')}}">
                    <input type="hidden" name="location" class="location" value="Lokasi tidak terdeteksi">
                    <input type="hidden" name="latitude" class="latitude" value="0">
                    <input type="hidden" name="longitude" class="longitude" value="0">
                    <button type="submit" class="btn btn-warning">Check Out</button>
                </form>
                <div style="margin-top: 30px; overflow: auto">
                    <span class="label label-primary">
                        <i class="fa fa-map-marker m-r-5" aria-hidden="true"></i>
                        <span id="location"></span>
                    </span>
                </div>

            </div>

            <div class="col-sm-12 m-t-10">
                <div class="col-sm-6">
                    <div class="panel-group" id="accordion1">
                        @foreach ($data as $key => $item)
                        @if ($item->timeEventType->id ==3)
                        <div class="panel panel-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1{{$key}}">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        {{ $item->check_date->format('d M Y') }} : {{ $item->check_time }}
                                        (<strong> {{ $item->timeEventType->description }} </strong>)
                                        @component('components._stage-description', [
                                        'class' => $item->stage->class_description,
                                        'description' => $item->stage->description
                                        ])
                                        @endcomponent
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1{{$key}}" class="panel-collapse collapse">
                                <div class="panel-body" style="border-style: solid; 
                                        border-color: {{ $item->timeEventType->id == 3 ? '#009999' : '#cc7a00' }};">
                                    <strong>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        {{ $item->check_date->format('d M Y') }} : {{ $item->check_time }}
                                    </strong> <br>
                                    <strong>
                                        <i class="fa fa-user" aria-hidden="true"></i> {{ $item->employee->name }}
                                        <span
                                            class="label label-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                                            {{ $item->personnel_no }}
                                        </span>
                                    </strong> <br>
                                    <strong>
                                        <div style="overflow: auto">
                                            <span
                                                class="label label-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                                                <i class="fa fa-map-marker m-r-5" aria-hidden="true"></i>
                                                {{ $item->location }}
                                            </span>
                                        </div>
                                    </strong>
                                    <hr>
                                    <strong>
                                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                                        {{ $item->wfhApproval->first()->employee->name }}
                                        <span
                                            class="label label-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                                            {{ $item->wfhApproval->first()->user['personnel_no'] }}
                                        </span>
                                    </strong> <br>
                                    <strong>
                                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        ({{ str_limit($item->wfhApproval->first()->employee->position_name,20) }}) <br>
                                    </strong>
                                    @component('components._stage-description', [
                                    'class' => $item->stage->class_description,
                                    'description' => $item->stage->description
                                    ])
                                    @endcomponent

                                    <hr>
                                    @include('e_absence._modals_check')
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-group" id="accordion2">
                        @foreach ($data as $key => $item)
                        @if ($item->timeEventType->id == 4)
                        <div class="panel panel-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2{{$key}}">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        {{ $item->check_date->format('d M Y') }} : {{ $item->check_time }}
                                        (<strong> {{ $item->timeEventType->description }} </strong>)
                                        @component('components._stage-description', [
                                        'class' => $item->stage->class_description,
                                        'description' => $item->stage->description
                                        ])
                                        @endcomponent
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2{{$key}}" class="panel-collapse collapse">
                                <div class="panel-body" style="border-style: solid; 
                                        border-color: {{ $item->timeEventType->id == 3 ? '#009999' : '#cc7a00' }};">
                                    <strong>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        {{ $item->check_date->format('d M Y') }} : {{ $item->check_time }}
                                    </strong> <br>
                                    <strong>
                                        <i class="fa fa-user" aria-hidden="true"></i> {{ $item->employee->name }}
                                        <span
                                            class="label label-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                                            {{ $item->personnel_no }}
                                        </span>
                                    </strong> <br>
                                    <strong>
                                        <div style="overflow: auto">
                                            <span
                                                class="label label-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                                                <i class="fa fa-map-marker m-r-5" aria-hidden="true"></i>
                                                {{ $item->location }}
                                            </span>
                                        </div>
                                    </strong>
                                    <hr>
                                    <strong>
                                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                                        {{ $item->wfhApproval->first()->employee->name }}
                                        <span
                                            class="label label-{{ $item->timeEventType->id == 3 ? 'info' : 'warning' }}">
                                            {{ $item->wfhApproval->first()->user['personnel_no'] }}
                                        </span>
                                    </strong> <br>
                                    <strong>
                                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        ({{ str_limit($item->wfhApproval->first()->employee->position_name,20) }}) <br>
                                    </strong>
                                    @component('components._stage-description', [
                                    'class' => $item->stage->class_description,
                                    'description' => $item->stage->description
                                    ])
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-sm-12 text-center">
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>

@endcomponent
<!-- end page container -->
@endsection

@push('styles')
<!-- Pace -->
<script src={{ url("/plugins/pace/pace.min.js") }}></script>
@endpush

@push('plugin-scripts')
<!-- Selectize -->
{{-- <script src={{ url("/plugins/selectize/selectize.min.js") }}></script> --}}
@endpush

@push('custom-scripts')
@if (session('check-in'))
<script>
    $('#myModal').modal('show');
</script>
@endif
<script>
    function getLocation() {
        options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };

        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition, error, options);
        } else { 
            alert("Geolocation is not supported by this browser.");
        }
    }

    function error(err) {
        console.warn('ERROR(' + err.code + '): ' + err.message);
    }

    function showPosition(position) {
        
        // sessionStorage.setItem("latitude", position.coords.latitude)
        // sessionStorage.setItem("longitude", position.coords.longitude)

        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        
        // if(latitude && longitude){
        //     $.get("{{ route('e-absence.location') }}/"+longitude+"/"+latitude, function(res){
        //         sessionStorage.setItem("location", res);
        //         $("#location").html(sessionStorage.getItem("location"));
        //         $(".location").val(sessionStorage.getItem("location"));
        //         $(".latitude").val(latitude);
        //         $(".longitude").val(longitude);
        //     });
        // }
        
        $.get("{{ route('e-absence.location') }}/"+longitude+"/"+latitude, function(res){
            sessionStorage.setItem("location", res);
            // sessionStorage.setItem("location", res.display_name);
            $("#location").html(sessionStorage.getItem("location"));
            $(".location").val(sessionStorage.getItem("location"));
            $(".latitude").val(latitude);
            $(".longitude").val(longitude);
        });
    }

    function setValue() {
        var location = sessionStorage.getItem("location");
        var latitude = sessionStorage.getItem("latitude");
        var longitude = sessionStorage.getItem("longitude");

        $("#location").html(location);
        $(".location").val(location);
        $(".latitude").val(latitude);
        $(".longitude").val(longitude);
    }
</script>
@endpush

@push('on-ready-scripts')
getLocation();
@endpush